BEGIN
readln(xh, xk, hx, yh, yk, hy);
nx := trunc((xk-xh)/hx)+1);
ny := trunc((yk-yh)/hy)+1);
x := xh // f(xh,y)
for i:=1 to nx do
begin
     writeln('x = ', x:4:1);
     writeln();
     y := yh;
     for j:=1 to ny do
     begin
          z := f(x,y);
          writeln('y = ', y:5:2, 'z = ', z);
          y := y + hy;
     end;
     writeln();
     x := x + hx;
end;
readln;
END.