uses
  crt;

const
  s1 = 'tck';
  s2 = '3pt';

var
  p1, p: string;
  i, l: integer;

begin
  clrscr;
  readln(p1);
  p := p1;
  l := length(p1);
  
  for i := 2 to l do
    if (p[i] = p[i - 1]) and (p[i] = ' ')
      then delete(p, i - 1, 1);
  
  for i := 1 to length(p * 2) do
  begin
    case p[i] of
      '.':
        begin
          delete(p, i, 1);
          insert(s1, p, i);
        end;
      ',':
        begin
          delete(p, i, 1);
          insert(s1, p, i);
        end;
    end;
  end;
  writeln(p);
  readln();
end.