program ProgramName;
uses crt;

var
	sum, sumsk, sk: Real;

BEGIN
	writeln('Введите сумму покупки');
	readln(sum);
	if sum > 1000 then
		begin
			sk := sum * 0,03;
			sumsk := sum - sk;
			writeln('Сумма скидки',sk:7:2);
			writeln('Покупатель заплатил',sumsk:7:2);
		end
	else
		writeln('Покупатель заплатил',sum:7:2);
	readln;
END.