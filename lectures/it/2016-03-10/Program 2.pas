program st;
uses crt;

type nat = 1..68;
var a, b : real;
    k : nat;

function stepen(x : real; n : nat): real;
    var i : integer; y : real;
    begin
        y := 1;
        for i:=1 to n do y := y * x;
        stepen := y
    end;

BEGIN
    readln(a, k);
    b := stepen(2.7, k) + 1/stepen(a+1);
    writeln('b = ', b);
    readln;
END.