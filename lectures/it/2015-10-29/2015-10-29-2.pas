program P2;

uses
  crt;

var
  a, b, h, x, y: real;

begin
  clrscr;
  readln(a, b, h);
  x := a;
  repeat
    y := 2 * x + 3; {f(x)}
    writeln('x = ', x:4:2, ' y = ', y);
    x := x + h;
  until x > b;
  readln;
  
end.