program P4;

uses
  crt;

var
  i, n: Integer;
  a, b, h, x, y: Real;

begin
  clrscr;
  readln(a, b, h);
  n := trunc((b - a) / h) + 1;
  x := a;
  for i := 1 to n do
  begin
    y := f(x);
    writeln('x = ', x:4:2, 'y = ', y);
    x := x + h;
  end;
  readln;
end.