\section{Методы анализа систем и принятия решений}

Рассмотрим некоторые методы анализа систем. Они делятся на качественные и количественные. 

Качественные методы используются на начальных этапах моделирования, если реальная система не может быть выражена в количественных характеристиках -- отсутствует описание закономерности систем в виде аналитических зависимостей. В результате такого моделирования разрабатывается концептуальная модель системы.

Количественные методы используются на последующих этапах моделирования для количественного анализа системы и формирования решений на основе разработанной модели.

\subsection{Методы качественного анализа систем}

К основным методам качественной оценки систем относят:

\begin{enumerate}
	\item метод типа "`мозговой атаки"' или "`коллективной генерации идей"'
	\item методы типа "`сценариев"'
	\item методы типа "`экспертных оценок"'
	\item методы типа "`дельфи"'
	\item методы типа "`дельфи"'
	\item методы типа "`дерева целей"'
\end{enumerate}

\paragraph{"`Мозговая атака"'}

"`Мозговая атака"' получила широкое распространение в начале 50-х годов, как метод тренировки мышления, нацеленный на открытие новых идей и достижения согласия группы людей на основе интуитивного мышления. Обычно при проведении сессии коллективной генерации идей (КГИ) стараются выполнять определенные правила:
\begin{itemize}
	\item обеспечить как можно большую свободу мышления участников КГИ и в высказывании ими новых идей;
	\item не допускать критики любой идеи, не объявлять её ложной и не прекращать обсуждения;
	\item желательно высказывать как можно больше идей, особенно нетривиальных;
\end{itemize}

Подобием сессии КГИ можно считать разного рода совещания -- конструктораты, заседания научных советов по проблемам, заседания специально создаваемых временных комиссий и другие собрания компетентных специалистов.

\paragraph{Методы типа "`Сценариев"'}

Методы подготовки и согласования представлений о проблеме или анализируемом проекте, изложенные в письменном виде, получили название "`Сценарий"'.

Первоначально этот метод предполагал подготовку текста, содержащего логическую последовательность событий или возможные варианты решения проблемы, упорядоченные по времени. Однако требование временных координат позднее было снято, и "`Сценарием"' стали называть любой документ, содержащий анализ рассматриваемой проблемы или предложения по её решению независимо от того, в какой форме он представлен.

Сценарий не только предусматривает содержательное рассуждения, которые помогают не упустить детали, но и содержит результаты количественного, технико-экономического или статистического анализа с предварительными выводами, которые можно получить на их основе.

Понятие "`Сценариев"' расширяется в направлении как областей применения, так и форм обсуждения, и методов их разработки. В "`Сенарий"' не только вводятся количественные параметры и устанавливаются их взаимосвязи, но и предлагаются методики составления "`Сценариев"' с использованием ЭВМ.

"`Сценарий"' является предварительной информацией, на основе которой производится дальнейшая работа по прогнозированию или разработке вариантов проекта. Таким образом, "`Сценарий" позволяет составить представление о проблеме, а затем приступить к более формализованному представлению системы в виде графиков и таблиц для проведения других методов системного анализа.

\paragraph{Методы экспертных оценок}

При использовании экспертных оценок обычно предполагается, что мнение группы экспертов надежнее, чем мнение отдельного эксперта. В некоторых теоретических исследованиях отмечается, что это предположение не является очевидным, но одновременно утверждается, что при соблюдении определенных требований, в большинстве случаев групповые оценки надежнее индивидуальных. К числу таких требований относятся:

\begin{enumerate}
	\item распределение оценок, полученных от экспертов, должно быть "`гладким";
	\item две групповые оценки, данные двумя одинаковыми подгруппами, выбранными случайным образом, должны быть близки;
\end{enumerate}

Все множество проблем, решаемых методом экспертных оценок, делится на два класса. 

К первому классу относятся такие, в отношении которых имеется достаточное обеспечение информацией. При этом методы опроса и обработки основываются на использовании принципа "`хорошего измерителя", т.е. эксперт -- источник достоверной информации, а групповое мнение экспертов близко к истинному решению. 

Ко второму классу относятся проблемы, в отношении которых знаний для уверенности и справедливости указанных гипотез недостаточно. В этом случае экспертов нельзя рассматривать как "`хороших измерителей", и необходимо осторожно подходить к результатам обработке результатов экспертизы.

Этапы экспертизы:
\begin{enumerate}
	\item формирование цели;
	\item разработка процедуры экспертизы;
	\item формирование группы экспертов;
	\item опрос;
	\item анализ и обработка информации.
\end{enumerate}

При формировке цели экспертизы, разработчик должен выработать четкое представление о том, кем и для каких целей будут использованы результаты. При обработке материалов коллективной экспертной оценки используются методы теории "`Ранговой корреляции"'. Для количественной оценки степени согласованности мнений экспертов применяется коэффициент конкардации, который позволяет оценить, насколько согласованны между собой ряды предпочтительности, построенные каждым экспертом:
\[
    0 < W < 1
\]
где $0$ означает полную противоположность, а $1$ -- полное совпадение ранжировок. 

Практически достоверность считается хорошей, если $0.7 < W < 0.8$. Небольшое значение коэффициента конкардации свидетельствующее о слабой согласованности мнений экспертов является следствием того, что а рассматриваемой совокупности экспертов действительно отсутствует общность мнений, или внутри рассматриваемой совокупности экспертов существуют группы с высокой согласованностью мнений, однако, обобщенные мнения таких групп противоположны.

Для наглядности представления о степени согласованности мнений двух любых экспертов служат коэффициенты парной ранговой корреляции $p$:
\[
    -1 < p < +1
\]
где $+1$ соответствует полному совпадению оценок в рамках двух экспертов, $-1$ -- двум взаимно противоположным ранжировкам важности свойств.

Тип используемой процедуры экспертизы зависит от задачи оценки. К наиболее употребимым процедурам оценки экспертного мнения относятся:
\begin{enumerate}
	\item ранжирование;
	\item парное сравнение;
	\item множественное сравнение;
	\item непосредственная оценка;
	\item Черчмена-Акоффа;
	\item метод фон Неймана-Моргенштерна
\end{enumerate}

При описании каждого из перечисленных методов будет предполагаться, что имеется конечное число измеряемых или оцениваемых альтернатив (объектов) $A = {a_1, \ldots, a_n}$ и сформулированы один или несколько признаков сравнения, по которым осуществляется процесс сравнения свойств объектов. Следовательно, методы измерения будут различаться лишь процедурой сравнения объектов.

\subparagraph{Ранжирование}

Метод представляет собой процедуру упорядочения объектов, выполняемую экспертом. На основе знаний и опыта эксперт располагает объекты в порядке предпочтения, руководствуясь одним или несколькими показателями сравнения. В зависимости от вида отношений между объектами, возможны различные варианты упорядочения объектов. 

В практике ранжирования чаще всего применяется числовое представление последовательности в виде натуральных чисел. Числа $x_1, \ldots, x_n$ в этом случае называются рангами и обозначаются как $r_1, \ldots, r_n$.