### Информатика 11 класс формулы
[toc]

#### Определение
> $\neg$ - логическое отрицание
> $\wedge$ - логическое умножение/конъюкция/**И**
> $\vee$ - логическое сложение/дизъюнкция/**ИЛИ**

#### Аксиомы
- $\bar{\bar x}=x$
- $x\vee \bar x =1$
- $x\vee1 =1$
- $x\vee x =x$
- $x\vee0 =x$
- $x\wedge\bar x =0$
- $x\wedge x =x$
- $x\wedge 0 =0$
- $x\wedge1 =x$

#### Таблицы истинности
|$a$|$b$|$a\wedge b$|$a\vee b$|
|:---:|:---:|:---:|:---:|
|$0$|$0$|$0$|$0$|
|$0$|$1$|$1$|$0$|
|$1$|$0$|$1$|$0$|
|$1$|$1$|$1$|$1$|

#### Дистрибутивность
- $x\wedge(y \vee z)=(x\wedge y)\vee(x\wedge z)$
- $x\vee(y \wedge z)=(x\vee y)\wedge(x\vee z)$

#### Законы де Моргана
- $\overline{x\wedge y}=\bar x \vee \bar y$
- $\overline{x\vee y}=\bar x \wedge \bar y$

#### Законы поглощения
- $x\wedge(x\vee y)=x$
- $x\vee(x\wedge y)=x$