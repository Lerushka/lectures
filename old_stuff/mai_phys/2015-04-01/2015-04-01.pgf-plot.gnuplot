set table "2015-04-01.pgf-plot.table"; set format "%.5f"
set samples 100.0; plot [x=-pi/3:3*pi/2] -cos(3*x+1)
