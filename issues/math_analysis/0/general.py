# -*- coding: UTF-8 -*-

# -------------
'''
Подключение необходимых библиотек и настройка параметров графика
'''
import matplotlib.pyplot as plt
from matplotlib import rcParams
from numpy import arange, pi, sin, cos

rcParams['figure.subplot.hspace'] = 0.5
rcParams['figure.figsize'] = (11.69, 8.27)
rcParams['font.family'] = 'fantasy'
rcParams['font.fantasy'] = 'Arial'

fig = plt.figure()
# -------------

# -------------
def plot_setup():
	'''
	эта функция подготавливает оси: делает начало отсчета в центре,
	наносит значения, включает сетку
	'''
	# plt.axis('equal')
	plt.axis('tight')

	X_limits = (-9.5, 9.5) # ограничение показываемого участка OX
	X_ticks_values = range(-9, 10) # ряд значений, которые будут отмечены на OX от -9 до 9
	X_ticks_labels = [r'$%d$' % x for x in range(-9, 10)] # отметки на OX

	y_limits = (-1.5, 1.5) # ограничение показываемого участка OY
	y_ticks_values = range(-1,2,2) # ряд значений, которые будут отмечены на OY от -1 до 1
	y_ticks_labels = [r'$%d$' % x for x in range(-1,2,2)] # отметки на OY (кроме 0)

	ax = plt.gca()
	ax.grid() # включение сетки

	ax.xaxis.set_ticks_position('bottom') # это все - настройка осей
	ax.yaxis.set_ticks_position('left') # просто магия, я сам уже не помню, как оно работает
	ax.spines['right'].set_color('none') # этот кусок кода у меня по графикам гуляет
	ax.spines['top'].set_color('none') # года так с 2014 :D
	ax.spines['bottom'].set_position(('data', 0))
	ax.spines['left'].set_position(('data', 0))
	
	plt.xlim(X_limits) # устанавливаем ограничение по OX
	plt.ylim(y_limits) # устанавливаем ограничение по OY
	plt.xticks(X_ticks_values, X_ticks_labels) # наносим значения на OX
	plt.yticks(y_ticks_values, y_ticks_labels) # наносим значения на OY
# -------------

# -------------
def plot_function():
	'''
	Эта функция выполняет построение функции и вы
	'''
	params = {'color': 'b', 'linewidth': 4, 'zorder': 3}

	X_function = [arange(-9,-6,0.1), arange(-6,-5,0.1), arange(-5,-3,0.1),
		arange(-3,0,0.1), arange(0,1,0.1), arange(1,3,0.1),
		arange(3,6,0.1), arange(6, 7,0.1), arange(7,9,0.1)]

	y_function = [[1]*len(arange(-3,0,0.1)), [1]*len(arange(0,1,0.1)), [0]*len(arange(1,3,0.1))] * 3

	for X, y in zip(X_function, y_function):
		plt.plot(X, y, **params)

	X_dots = [-9, -6, -5, -5, -3, -3, 0, 1, 1, 3, 3, 6, 7, 7, 9]
	y_dots = [1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0]

	plt.plot(X_dots, y_dots, 'wo', zorder=3)
# -------------


def plot_fourier(N, color):
	X = arange(-9, 9, 0.001)
	y = []

	a_0 = 4/3
	a_n = lambda n: (1/(pi*n)) * ( sin(pi*n/3) ) * cos(pi*n*x/3)
	b_n = lambda n: (1/(pi*n)) * ( (-1)**n - cos(pi*n/3) ) * sin(pi*n*x/3)

	for x in X:
		y.append(a_0/2 + sum(map(a_n, N)) + sum(map(b_n, N)))

	# plt.plot(X, y, color, zorder=4)

	label = u'Сумма %d гармоник'%(len(N))
	plt.plot(X, y, color, zorder=4, label=label, lw=0.7)

plots = [1, 2, 3]
N = [2, 10, 100]
colors = ['y', 'g', 'r']

# plt.suptitle(u'Разложение в общий ряд')
# for plot, n, color in zip(plots, N, colors):
# 	plt.subplot(3, 1, plot)
# 	plt.title(u'Сумма %d гармоник'%(n))
# 	plot_setup()
# 	plot_function()
# 	plot_fourier(arange(1, n), color)

plot_setup()
plot_function()
for n, color, in zip(N, colors):
	plot_fourier(range(1, n), color)

plt.legend(loc='lower right', prop={'size':10})

# plt.show()

fig.savefig('general_merge.png', dpi=800)
