function [ y ] = integral_f( x )
    y = x * cos(x^3);
end