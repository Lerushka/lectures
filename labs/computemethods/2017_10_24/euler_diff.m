function [y] = euler_diff(f, x_0, y_0, h, n)
    y = zeros(2, n+1);
    y(1, 1) = x_0;
    y(2, 1) = y_0;
    for i = 2 : 1 : n+1
        y(1, i) = x_0 + (i - 1) * h;
        y(2, i) = y(2, i-1) + h * f(y(1, i-1), y(2, i-1));
    end
end
