clear, clc
f = @(x, y) 0.324 * (x^2 + sin(1.5 * x)) + 0.612 * y;

x_0 = 0.2; X = 1.2;
p = 1; eps = 0.1;
y_0 = 0.25;

n_1 = 1;
h = (X - x_0) / n_1;
y_1 = euler_diff(f, x_0, y_0, h, n_1);

n_2 = 2;
h = (X - x_0) / n_2;
y_2 = euler_diff(f, x_0, y_0, h, n_2);

yh_1 = y_1(2, :);
yh_2 = y_2(2, 1:2:(n_2 + 1));

R = norm(yh_1 - yh_2) / (2^p - 1);

while (R >= eps)
    y_1 = y_2;
    n_1 = n_2;
    n_2 = 2 * n_1;
    h = (X - x_0) / n_2;
    y_2 = euler_diff(f, x_0, y_0, h, n_2);
    yh_1 = y_1(2, :);
    yh_2 = y_2(2, 1:2:(n_2 + 1));
    R = norm(yh_1 - yh_2) / (2^p - 1);
end
n_2
y_2(1, :)
y_2(2, :)

[x, y] = ode45(f, [x_0, X] ,y_0);

plot(y_2(1, :), y_2(2, :),'r', x, y, 'g')
legend('Approximate solution', 'Exact solution')
