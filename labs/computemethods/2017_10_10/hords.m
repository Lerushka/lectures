function [b] = hords(f, a, b, eps)

	while (abs(b - a) > eps)
		a = b - (b - a) * f(b) / (f(b) - f(a));
		b = a - (a - b) * f(a) / (f(a) - f(b));
	end	

end