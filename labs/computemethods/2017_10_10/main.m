%ok<*NOPTS>

%%
fplot(func, [-5 5])
fplot(func, [0 1])
fplot(func, [-1 1])
%%
a = 0; b = 1;
eps = 0.01;

%%
f = @(x) cos(x) - 2 * x^2 + 0.5;
df = @(x) - 4*x - sin(x);
ddf = @(x) - cos(x) - 4;

%%
dichotomy(f, a, b, eps)

%%
hords(f, a, b, eps)

%%
newtons(f, df, ddf, a, b, eps)

%%
iterations(a, b, -0.1, eps)