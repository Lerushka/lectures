function [x] = iterations(a, b, lambda, eps)

    f = @(x) x - lambda * (cos(x) - 2 * x^2 + 0.5);
    
    x_0 = b - a;
    x = f(x_0);
    
    while ( abs(x - x_0) > eps )
        x_0 = x;
        x = f(x_0);
    end
    
end