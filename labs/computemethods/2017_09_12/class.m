%% variables
x = [0.43, 0.48, 0.55, 0.62, 0.7, 0.75];
y = [1.63597, 1.73234, 1.87686, 2.03345, 2.22846, 2.35973];

n = length(x);

t = [0.512, 0.608, 0.702];

%% wtf
xi = x(1) : 0.02 : x(length(x));
yil = interp1(x, y, xi, 'linear');
yis = interp1(x, y, xi, 'spline');
yin = interp1(x, y, xi, 'nearest');

%% plotting
plot(x, y, 'bo', xi, yin, '-r', xi, yil, ':b', xi, yis, '+k');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'r');
ylabel('y', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'r');
legend('экспериментальные значения', 'nearest', 'cubic', 'spline');
