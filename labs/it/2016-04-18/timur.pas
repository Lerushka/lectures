PROGRAM semester_2_timur_1;

USES
	crt;

VAR
	j, i, k: integer;
	s, w: string;

BEGIN
	j := 1;
	readln(s);
		
	for i := 1 to length(s) do
		if s[i] = ' ' then begin
			w := copy(s, j, i - j);
			for k := 1 to length(w) do if w[k] = 'a' then  w[k] := 'o';
			j := j + length(w);
			write(w, ' ');
		end;
	readln;
END.