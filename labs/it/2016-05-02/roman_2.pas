program Roman_2;
uses crt;

var
	a, b, s, t: real;

procedure y(a, b: real; var result: real);
begin
	result := (Sqr(a) / (1 + b)) + (Sqr(b) / (1 + a)) - Sqr(a + b);
end;

BEGIN
	readln(s, t);
	y(s, t, a);
	y(t-s, s*t, b);
	writeln(a + Sqr(b));
END.