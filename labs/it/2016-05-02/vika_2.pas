// z(3t+1, -2s, 1.8t) + z(2.6, s, 3t+s)
//               |a|+|b|+|c|
// z(a,b,c) = -----------------
//             a^2 + b^3 + c^4

uses crt;

var a, b, s, t: real;

procedure z(a, b, c: real; var result: real);
begin
    result := (abs(a)+abs(b)+abs(c))/(a*a + b*b*b + c*c*c*c);
end;

BEGIN
readln(s, t);
z(3*t+1, -2*s, 1.8*t, a);
z(2.6, s, 3*t+s, b);
writeln(a + b);
END.