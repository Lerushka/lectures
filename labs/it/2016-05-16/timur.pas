﻿program Timur_3;

uses
	crt;

const
	nmax = 30;

type
	entry = record
		surname: string;
		name: string;
		phone: string;
	end;
	
	mass = array[1..nmax] of entry;

var
	entries: mass;
	menu: integer;

function MainMenu: integer;
var menu: integer;
begin
	writeln('1 - Заполнить базу данных из файла');
	writeln('2 - Добавить запись в базу данных');
	writeln('3 - Вывести базу данных на экран');
	writeln('4 - Поиск по фамилии');
	writeln('0 - Выход');
	readln(menu);
	MainMenu := menu;
end;

procedure read_from_file(var entries: mass);
var file_in: text;
	i: integer := 1;
begin
	assign(file_in, 'input_timur.txt');
	reset(file_in);
	while not eof(file_in) do
	begin
		with entries[i]	do
			with entry do 
			begin
				readln(file_in, surname);
				readln(file_in, name);
				readln(file_in, phone);
			end;
		i := i + 1;
	end;
	close(file_in);
end;

procedure add_entry(var entries: mass);
var i: integer;
begin
	clrscr;
	for i := 1 to nmax do
		with entries[i] do
			with entry do
				if surname = '' then begin
					writeln('Добавление нового студента');
					write('Фамилия: '); readln(surname);
					write('Имя: '); readln(name);
					write('Номер телефона: '); readln(phone);
					break;
				end;
end;

procedure screen_output(var entries: mass);
var i: integer;
begin
	clrscr;
	writeln('Список группы');
	writeln('|      Фамилия      |     Имя    |      Номер      |');
	for i := 1 to nmax do
		with entries[i] do
			with entry do
				if not (surname = ('')) then
					writeln('| ', surname:18, '|', name:12, '|', phone:17, '|');
	readln();
end;

procedure search(var entries: mass);
var surname_: string;
	i, n: integer;
	j: integer := 0;
begin
	clrscr;
	for n := 1 to nmax do if entries[n].surname = '' then break;
	writeln('Введите фамилию студента');
	readln(surname_);
	for i := 1 to n do
		with entries[i] do
			with entry do
				case surname = surname_ of
					True: if phone = '' then begin
							writeln('У студента ', surname, ' ', name, ' не указан номер телефона');
							break;
						end else begin
							writeln('Найден студент ', surname, ' ', name);
							writeln('Его номер: ', phone);
						end;
					False: j := j + 1;
				end;
				if j = n then writeln('Студент ', surname_, ' не найден');
	readln();
end;

BEGIN
	repeat
		clrscr;
		menu := MainMenu;
		case menu of
			1: read_from_file(entries);
			2: add_entry(entries);
			3: screen_output(entries);
			4: search(entries);
		end;
	until menu = 0;
END.