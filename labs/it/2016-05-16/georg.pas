program Roman_3;

uses
	crt;
// ограничение на максимальное количество человек: 30
const
	nmax = 30;
// создаем запись на 30 элементов с тремя текстовыми и одним вещественным значениями
type
	entry = record
		surname: String;
		name: String;
		sex: String;
		mark: Real;
	end;
	
	mass = array[1..nmax] of entry;
// немного переменных
var
	entries: mass;
	menu: integer;

function MainMenu: integer;
// эта функция отвечает за отображение главного меню
var
	menu: integer;
begin
	writeln('1 - Прочитать базу данных из файла');
	writeln('2 - Заполнить базу данных');
	writeln('3 - Вывести базу данных на экран');
	writeln('4 - Дополнить базу данных');
	writeln('5 - Удалить учащихся с оценкой < 2.5');
	writeln('0 - Выход');
	readln(menu);
	MainMenu := menu;
end;


procedure read_from_file(var entries: mass);
{эта функция ПОСТРОЧНО читает файл input.txt,
который лежит в одной папке с программой
здесь надо отметить, что я не очень разобрался с текстовыми файлами
поэтому они должны быть заполнены следующим образом:
фамилия 1
имя 1
пол (м/ж) 1
средний балл 1
фамилия 2
имя 2
пол (м/ж) 2
средний балл 2
ТОЛЬКО так, каждое слово на отдельной строке
средний балл, если дробный, разделять точкой
между данными двух разных людей не должно быть разрывов строки
увы, костыль, но я не придумал, как лучше}
var
	file_in: text;
	i: integer;
begin
	assign(file_in, 'input.txt');
	reset(file_in);
	i := 1;
	while not eof(file_in) do
	begin
		with entries[i]	do
			with entry do 
			begin
				readln(file_in, surname);
				readln(file_in, name);
				readln(file_in, sex);
				readln(file_in, mark);
			end;
		i := i + 1;
	end;
	close(file_in);
end;

procedure screen_output(var entries: mass);
{эта функция выводит базу данных на экран, тоже построчно
можно отметить, что делает она это до тех пор, пока не встречает
пустое поле фамилии, на нем она останавливается
это сделано на случай, если заполнено не 30 записей, а меньше}
var i: integer;
begin
	clrscr;
	writeln('Список группы');
	writeln('|      Фамилия      |   Имя   |  Пол  | Средний балл |');
	for i := 1 to nmax do
	begin
		with entries[i] do
			with entry do
				if not (surname = ('')) then
					writeln('| ', surname:18, '|', name:9, '|', sex:7, '|', mark:14, '|');
	end;
	readln();
end;

procedure enter(var entries: mass);
{эта функция предназначена для ручного заполнения базы данных
предлагается указать количество человек, которых ты добавишь
далее вводятся данные на каждого из них, все вроде просто}
var i, n: integer;
begin
	clrscr;
	write('Введите количество студентов <= 20: ');
	readln(n);
	for i := 1 to n do
	begin
		writeln('Введите информацию о ', i, ' студенте');
		write('Фамилия: ');
		readln(entries[i].surname);
		write('Имя: ');
		readln(entries[i].name);
		write('Пол (м|ж): ');
		readln(entries[i].sex);
		write('Средний балл за сессию: ');
		readln(entries[i].mark);
	end;
end;

procedure add_entry(var entries: mass);
{эта функция нужна для добавления одного студента в конец всей базы
запись произведется в первую найденную ячейку, у которой будет пусто
поле с фамилией (просто критерий везде один и тот же, для удобства)}
var i: integer;
begin
	clrscr;
	for i := 1 to nmax do 
	begin
		if entries[i].surname = '' then
		begin
			writeln('Добавление нового студента');
			write('Фамилия: ');
			readln(entries[i].surname);
			write('Имя: ');
			readln(entries[i].name);
			write('Пол (м|ж): ');
			readln(entries[i].sex);
			write('Средний балл за сессию: ');
			readln(entries[i].mark);
			break;
		end;
	end;
end;

procedure remove(var entries: mass);
{собственно, твое задание - удаление всех студентов с баллом ниже 2.5
вообще ничего сложного, ты просто определяешь, сколько у тебя всего записей
(на случай, если их меньше 30), потом проверяешь поле "оценка" у каждой такой
записи. если значение этого поля меньше 2.5, то мы ее удаляем нафиг. красота.}
var i, j, n: integer;
begin
	clrscr;
	for n := 1 to nmax do if entries[n].surname = '' then break;
	i := 1;
	while i <= n do
	begin
		if entries[i].mark <= 2.5 then begin
			for j := i to n - 1 do entries[j] := entries[j + 1];
			n := n - 1;
		end
		else
			i := i + 1;
	end;
	readln();
end;

BEGIN
{а это главный цикл программы, в котором крутится главное меню
на выбор 5 опций -- все они вызывают конкретную функцию.
ахтунг, на кнопку 0 повешен выход
предполагается, что ты сначала заполнишь базу данных одним из трех способов
потом можно вывести ее на экран для демонстрации
затем применить функцию удаления и вывести базу на экран еще раз}
	repeat
		clrscr;
		menu := MainMenu;
		case menu of
			1: read_from_file(entries);
			2: enter(entries);
			3: screen_output(entries);
			4: add_entry(entries);
			5: remove(entries);
		end;
	until menu = 0;
END.