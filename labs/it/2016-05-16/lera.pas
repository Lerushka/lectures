program Lera;

uses
	crt;

const
	nmax = 20; // ����������� �� ������������ ���������� �������: 20

type // ������� ������ �� 30 ��������� � �������� ���������� � ����� ������������� ����������
	entry = record
		surname: string;
		name: string;
		patronymic: string;
		sex: string;
		check: integer; // ��� ���������� ������������ ��� ����, ����� ������,
						// ������� ��� ��� ����������� � ��
	end;
	
	mass = array[1..nmax] of entry;

var
	entries: mass;
	menu: integer;

function MainMenu: integer;
{��� ������� ���������� �������� �������� ���� � ���������
��������, ��������� � ����������}
var menu: integer;
begin
	writeln('1 - ��������� ���� ������ �� �����');
	writeln('2 - ������� ���� ������ �� �����');
	writeln('3 - ����� ����� ���������� ����');
	writeln('0 - �����');
	readln(menu);
	MainMenu := menu;
end;

procedure read_from_file(var entries: mass);
{��� ������� ��������� ������ ���� input_lera.txt (��������),
������� ����� � ����� ����� � ����������
����� ���� ��������, ��� � �� ����� ���������� � ���������� �������
������� ��� ������ ���� ��������� ��������� �������:
������� 1
��� 1
�������� 1
��� (�/�) 1
������� 2
��� 2
�������� 2
��� (�/�) 2
������ ���, ������ ����� �� ��������� ������
����� ������� ���� ������ ����� �� ������ ���� �������� ������
���, �������, �� � �� ��������, ��� �����}
var file_in: text;
	i: integer := 1;
begin
	assign(file_in, 'input_lera.txt'); // ��� ����� �������� ���
	reset(file_in); // �������� ����� �� ������
	while not eof(file_in) do // ���� ����������� �� ����� �����
	begin
		with entries[i] do
			with entry do 
			begin
				readln(file_in, surname);
				readln(file_in, name);
				readln(file_in, patronymic);
				readln(file_in, sex);
			end;
		i := i + 1;
	end;
	close(file_in); // ���� �����������
end;

procedure screen_output(var entries: mass);
{��� ������� ������� ���� ������ �� �����, ���� ���������
����� ��������, ��� ������ ��� ��� �� ��� ���, ���� �� ���������
������ ���� �������, �� ��� ��� ���������������
��� ������� �� ������, ���� ��������� �� 20 �������, � ������
(� ����� �������� �� �����������, �� ��� �� :) )}
var i: integer;
begin
	clrscr;
	writeln('������ ������');
	writeln('|    �������    |     ���    |    ��������    | ��� |');
	for i := 1 to nmax do
		with entries[i] do
			with entry do
				if not (surname = ('')) then
					writeln('|', surname:15, '|', name:12, '|', patronymic:16, '|', sex:5, '|');
	readln();
end;


procedure search(var entries: mass);
var name_: string;
	i, j, n: integer;
begin
	clrscr;
	for n := 1 to nmax do if entries[n].surname = '' then break; // ��� ������� �����, �� ������,
																 // ���� ������� ������ 20, ����� �������
	for i := 1 to n do begin // ����� ��������� ���������� �������� ������ ����
		name_ := entries[i].name; // ������� ��������� ����������, �������� i-��� ���
		for j := 1 to n do // "��������" ������ ������
			with entries[j] do
				with entry do
					if name = name_ then check := check + 1 // ���� ��������� ��� ����� ����� � ������
															// �� ������� � ����� ����� ���������� �� 1
					else continue;	// ����� - ���������� ������
	end;

	for j := 1 to n - 1 do // ��������� ������ ������� �������� �� ������������ ���� (���� check)
		for i := 1 to n - j do // ��������� �� ����������� � �����������
			if entries[i].check > entries[i + 1].check then
				swap(entries[i], entries[i + 1]);

	for i := 1 to n do // ������ ������� ����� ���������� �����
	begin
		name_ := entries[i].name; //����� �������� ��������� ����������
		with entries[i + 1] do // ������ �� ������ ������� ��������� ������� �������
			with entry do // �.�. ��� ���� ������� ���������� ��� ������ ���� ���
				if (name <> name_) and (check >= 2) then // ���� � ���, ����� ������� ��������� �������������
														 // ���. ��������, � ��� ���� ����, ����, ����. ������� ����
														 // ������ ����������, ��� ����� � ��������� �� �����������.
														 // �� � ����� ����������� ���� ������ ����, ���� �� ���.
					writeln(name, ', ����������: ', check)
				else continue;
	end;
	readln();
end;

BEGIN
{� ��� ������� ���� ���������, � ������� �������� ������� ����
�� ����� 3 ����� -- ��� ��� �������� ���������� �������.
������, �� ������ 0 ������� �����
��������������, ��� �� ������� ��������� ���� ������
����� ����� �������� �� �� ����� ��� ������������
����� ��������� ������� ������ � �������, �.�. ��������� �����
��� ������ ����� ���� ����������� :blush:}
	repeat
		clrscr;
		menu := MainMenu;
		case menu of
			1: read_from_file(entries);
			2: screen_output(entries);
			3: search(entries);
		end;
	until menu = 0;
END.